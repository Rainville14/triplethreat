<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'triplethreat');

/** MySQL database username */
define('DB_USER', 'tthreat');

/** MySQL database password */
define('DB_PASSWORD', 'password1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ')92:0<fU>(*AWZ)qcbCsUBTN88xcxyaIX@dF2KM-DwX=P>h^pH0*G]lagY&`2/:a');
define('SECURE_AUTH_KEY',  'Cq )^xl- iV_;+;L{1jRDf6f0xvb0AY~dw(+hO,j_z!R!p58EqH3wy`=F]mg!wgh');
define('LOGGED_IN_KEY',    'O5>eRK+r=b/CLW/9r<L0q0},vpJTs=#[WXpk]<;4qjo/5^r_z0L+cu[)( YOtG,c');
define('NONCE_KEY',        'P; P*!M*p7s[|HHBXAL._I?-fV-ir#Ede:[2a4reQJwO(B:%i2adXLajtK.m!.}M');
define('AUTH_SALT',        'vo8(F@zTCH%45o4>W=i _fcAmy@5gEuoG7Rh9}{v?hKtmq:86-[dWx6jh/3v.||O');
define('SECURE_AUTH_SALT', 'Q/woaq:rhqZaNj#8op`3}O6+eDFFnMRF6E|($o_B^{U#uX;wdD9IOr^7,spHg|iR');
define('LOGGED_IN_SALT',   '{aV#(quHba`BwW4dM.uR5]~)8O1!tGz-A#nT~(>[B/@=I]^U.=4:e-Je(!p}f@<(');
define('NONCE_SALT',       '[+8O(1E+HW^*49|G;SB_kg&<SM>M6Z| YVBonBP?F5u])Xr5#?G-t3Da`tAFHl:Z');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
